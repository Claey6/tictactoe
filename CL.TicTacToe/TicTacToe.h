#pragma once

#include <iostream>

class TicTacToe
{
private:


	// fields
	char m_board[9] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int m_numTurns = 0;
	char m_playerTurn = 1;
	char m_winner;


public:

	// methods
	virtual void DisplayBoard()
	{
		std::cout << "\n";

		for (int count = 0; count <= 8; count++)
		{
			if (count == 2 || count == 5)
			{
				if (m_board[count] == 11)
				{
					std::cout << "X" << "\n" << "\n";
				}
				else if (m_board[count] == 12)
				{
					std::cout << "O" << "\n" << "\n";
				}
				else
				{
					std::cout << (count + 1) << "\n" << "\n";
				}
			}
			else if (m_board[count] == 11)
			{
				std::cout << "X" << " " << " ";
			}
			else if (m_board[count] == 12)
			{
				std::cout << "O" << " " << " ";
			}
			else
			{
				std::cout << (count + 1) << " " << " ";
			}

		}

		std::cout << "\n" << "\n";
	}

	virtual bool IsOver()
	{
		// horizontal
		if (m_board[0] == 11 && m_board[1] == 11 && m_board[2] == 11)
		{
			return true;
		}
		else if (m_board[0] == 12 && m_board[1] == 12 && m_board[2] == 12)
		{
			return true;
		}
		else if (m_board[3] == 11 && m_board[4] == 11 && m_board[5] == 11)
		{
			return true;
		}
		else if (m_board[3] == 12 && m_board[4] == 12 && m_board[5] == 12)
		{
			return true;
		}
		else if (m_board[6] == 11 && m_board[7] == 11 && m_board[8] == 11)
		{
			return true;
		}
		else if (m_board[6] == 12 && m_board[7] == 12 && m_board[8] == 12)
		{
			return true;
		}
		// vertical
		else if (m_board[0] == 11 && m_board[3] == 11 && m_board[6] == 11)
		{
			return true;
		}
		else if (m_board[0] == 12 && m_board[3] == 12 && m_board[6] == 12)
		{
			return true;
		}
		else if (m_board[1] == 11 && m_board[4] == 11 && m_board[7] == 11)
		{
			return true;
		}
		else if (m_board[1] == 12 && m_board[4] == 12 && m_board[7] == 12)
		{
			return true;
		}
		else if (m_board[2] == 11 && m_board[5] == 11 && m_board[8] == 11)
		{
			return true;
		}
		else if (m_board[2] == 12 && m_board[5] == 12 && m_board[8] == 12)
		{
			return true;
		}
		// diagonal
		else if (m_board[0] == 11 && m_board[4] == 11 && m_board[8] == 11)
		{
			return true;
		}
		else if (m_board[0] == 12 && m_board[4] == 12 && m_board[8] == 12)
		{
			return true;
		}
		else if (m_board[2] == 11 && m_board[4] == 11 && m_board[6] == 11)
		{
			return true;
		}
		else if (m_board[2] == 12 && m_board[4] == 12 && m_board[6] == 12)
		{
			return true;
		}
		else if (m_numTurns < 9)
		{
			return false;
		}

	}

	virtual char GetPlayerTurn()
	{
		if (m_playerTurn == 1)
		{
			return m_playerTurn;
		}
		else if (m_playerTurn == 2)
		{
			return m_playerTurn;
		}
	}

	virtual bool IsValidMove(int position)
	{

		if (m_board[position - 1] == 11 || m_board[position - 1] == 12)
		{
			std::cout << "\n" << "Spot taken, please choose another" << "\n" << "\n";
			return false;
		}
		else if (m_numTurns < 9)
		{
			return true;
		}
	}

	virtual void Move(int position)
	{
		if (m_playerTurn == 1)
		{
			m_board[position - 1] = 11;
			m_numTurns++;
			m_playerTurn = 2;
		}
		else if (m_playerTurn == 2)
		{
			m_board[position - 1] = 12;
			m_numTurns++;
			m_playerTurn = 1;
		}

	}

	virtual void DisplayResult()
	{
		// horizontal
		if (m_board[0] == 11 && m_board[1] == 11 && m_board[2] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[0] == 12 && m_board[1] == 12 && m_board[2] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[3] == 11 && m_board[4] == 11 && m_board[5] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[3] == 12 && m_board[4] == 12 && m_board[5] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[6] == 11 && m_board[7] == 11 && m_board[8] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[6] == 12 && m_board[7] == 12 && m_board[8] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		// vertical
		else if (m_board[0] == 11 && m_board[3] == 11 && m_board[6] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[0] == 12 && m_board[3] == 12 && m_board[6] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[1] == 11 && m_board[4] == 11 && m_board[7] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[1] == 12 && m_board[4] == 12 && m_board[7] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[2] == 11 && m_board[5] == 11 && m_board[8] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[2] == 12 && m_board[5] == 12 && m_board[8] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		// diagonal 
		else if (m_board[0] == 11 && m_board[4] == 11 && m_board[8] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[0] == 12 && m_board[4] == 12 && m_board[8] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[2] == 11 && m_board[4] == 11 && m_board[6] == 11)
		{
			m_winner = 1;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else if (m_board[2] == 12 && m_board[4] == 12 && m_board[6] == 12)
		{
			m_winner = 2;
			std::cout << "Player " << m_winner << " Wins!!";
		}
		else
		{
			std::cout << "The Game has Ended in a Tie.";
		}

	}

};

